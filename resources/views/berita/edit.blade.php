@extends('layout.master')

@section('judul')
  Ubah Berita
@endsection

@section('content')
<form action="/berita/{{$berita->id}}" method="post" enctype="multipart/form-data">
  @csrf
  @method('PUT')
  <div class="form-group">
    <label>Judul Berita</label>
    <input type="text" class="form-control" name="judul" value="{{$berita->judul}}">
  </div>
  @error('judul') 
  <div class="alert alert danger">{{$message}}</div>
  @enderror
  <div class="form-group">
    <label>Isi Berita</label>
    <textarea class="form-control" name="isi">{{$berita->isi}}</textarea>
    @error('isi') 
    <div class="alert alert danger">{{$message}}</div>
    @enderror
  </div>
  <div class="form-group">
    <label>Kategori</label>
    <select class="form-control" name="kategori_id">
      <option value="">---Pilih Kategori---</option>
      @foreach ($kategori as $item)
      @if($item->id === $berita->kategori_id)
          <option value="{{$item->id}}" selected>{{$item->nama_kategori}}</option>
          @else
          <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
          @endif
      @endforeach
    </select>
    @error('kategori_id') 
  <div class="alert alert danger">{{$message}}</div>
  @enderror
  </div>
  <div class="form-group">
    <label>Thumbnail</label>
    <input type="file" class="form-control" name="thumbnail">
  </div>
  @error('thumbnail') 
  <div class="alert alert danger">{{$message}}</div>
  @enderror
  <input type="submit" class="btn btn-primary" value="Perbarui">
  <a href="/berita" class="btn btn-secondary">Kembali</a>
</form>
@endsection