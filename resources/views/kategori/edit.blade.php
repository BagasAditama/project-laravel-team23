@extends('layout.master')

@section('judul')
  Ubah Kategori
@endsection

@section('content')
<form action="/kategori/{{$kategori->id}}" method="post">
  @csrf
  @method('PUT')
  <div class="form-group">
    <label>Nama Kategori</label>
    <input type="text" class="form-control" name="nama_kategori" value="{{$kategori->nama_kategori}}">
  </div>
  @error('nama_kategori') 
  <div class="alert alert danger">{{$message}}</div>
  @enderror
  <input type="submit" class="btn btn-primary" value="Perbarui">
  <a href="/kategori" class="btn btn-secondary">Kembali</a>
</form>
@endsection